package com.bankcli.entity;

import java.util.Date;


public class Transaction {
    public Date datetime;
    public User from;
    public User to;
    public int amount;

    public Transaction() {
        this.datetime = new Date();
    }
    public Transaction(User from, User to, int amount) {
        this.datetime = new Date();
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
