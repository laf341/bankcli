package com.bankcli.actions;

import com.bankcli.entity.User;
import com.bankcli.repository.UserRepo;

public class LoginAction extends Action {
    private UserRepo userRepo = new UserRepo();
    public LoginAction() {
        actionName = "Login";
        helloMessage = "Enter login:";
        nextAction = new MainAction("Login success!");
    }


    @Override
    public void run(){
        try {
            printHelloMessage();
            String username = bufferedReader.readLine();
            System.out.println("Enter password");
            String password = bufferedReader.readLine();

            userRepo.where("username", username);
            User user = userRepo.getOne();
            if (!user.password.equals(password)) {
                currentUser = null;
                this.error("Invalid login or password! Please try again");
            }
            nextAction.currentUser = user;
            nextAction.run();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


}
