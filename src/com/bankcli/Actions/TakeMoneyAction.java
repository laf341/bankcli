package com.bankcli.actions;

import com.bankcli.entity.Transaction;
import com.bankcli.repository.TransactionRepo;
import com.bankcli.repository.UserRepo;


public class TakeMoneyAction extends Action {
    private UserRepo userFrom;
    private TransactionRepo transactionRepo;

    public TakeMoneyAction() {
        actionName = "Take money";
        helloMessage = "Enter sum:";
    }

    public void run() {
        super.run();
        try {
            int sum = Integer.parseInt(bufferedReader.readLine());

            userFrom = new UserRepo();
            userFrom.where("username", "bank__");
            transactionRepo = new TransactionRepo(new Transaction(userFrom.getOne(), currentUser, sum));
            transactionRepo.save();

            nextAction = new MainAction("Taking completed. ");
            nextAction.currentUser = currentUser;
            nextAction.run();

        } catch (NumberFormatException e) {
            error("Invalid input format!");
        } catch (Exception ex) {

        }

    }
}
