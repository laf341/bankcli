package com.bankcli.actions;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import com.bankcli.entity.User;

public abstract class Action {
    protected User currentUser;
    protected String actionName;
    protected String helloMessage;
    protected String inputAction;
    protected Action nextAction;
    protected BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    protected Map<String, Action> actionMap = new HashMap<>();

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getHelloMessage() {
        return helloMessage;
    }

    public void setHelloMessage(String helloMessage) {
        this.helloMessage = helloMessage;
    }

    /*
     * Print helloMessage to Console
     */
    public void printHelloMessage(){
        System.out.println(getHelloMessage());
    }

    public void setActionInMap(String index, Action action){
        this.actionMap.put(index,action);
    }
    public Map<String, Action> getActionMap() {
        return this.actionMap;
    }
    public void printActions(){
        for (Map.Entry<String, Action> entry : this.actionMap.entrySet())
        {
            System.out.println(entry.getKey() + ". " + entry.getValue().getActionName());
        }
    }

    public void run() {
        printHelloMessage();
    }


    protected void error(String errorMessage) {
        System.out.println(errorMessage);
        this.run();
    }
}
