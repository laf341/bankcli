package com.bankcli.actions;

import java.util.HashMap;
import java.util.Map;

public class DefaultAction extends Action {
    private LoginAction loginAction = new LoginAction();
    private Map<String, Action> actionMap = new HashMap<>();
    {
        actionMap.put("y", loginAction);
        actionMap.put("n", this);
    }

    public DefaultAction() {
        helloMessage = "Dou you have existing account? (y/n)";
    }

    @Override
    public void run(){
        try {
            printHelloMessage();
            inputAction = bufferedReader.readLine();
            if (!actionMap.containsKey(inputAction)) {
                this.error("Invalid input! Enter 'y' or 'n'. Please try again.");
            }
            actionMap.get(inputAction).run();
        } catch (Exception ex) {
        }
    }
}
