package com.bankcli.actions;

public class ExportHistoryAction extends Action {
    private HistoryToCsvAction historyToCsvAction = new HistoryToCsvAction();
    private HistoryToScreenAction historyToScreenAction = new HistoryToScreenAction();

    public ExportHistoryAction() {
        actionName = "Export history";
        helloMessage = "Choose format:";
        setActionInMap("1", historyToScreenAction);
        setActionInMap("2", historyToCsvAction);
    }

    public void run() {
        try {
            printHelloMessage();
            printActions();
            inputAction = bufferedReader.readLine();
            if (!actionMap.containsKey(inputAction)) error("Invalid input! Action not found");
            actionMap.get(inputAction).currentUser = currentUser;
            actionMap.get(inputAction).run();
        } catch (Exception ex) {
        }

    }


}
