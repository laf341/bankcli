package com.bankcli.actions;


public class LogoutAction extends Action {
    public LogoutAction() {
        setActionName("Logout");

    }
    public void run(){
        currentUser = null;
        DefaultAction defaultAction = new DefaultAction();
        defaultAction.run();
    }
}
