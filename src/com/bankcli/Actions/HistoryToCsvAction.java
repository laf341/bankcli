package com.bankcli.actions;

import com.bankcli.csv.WriteCsv;
import com.bankcli.repository.TransactionRepo;

public class HistoryToCsvAction extends Action {
    private TransactionRepo transactionRepo;

    public HistoryToCsvAction() {
        actionName = "To CSV.";
    }

    public void run() {
        try {
            transactionRepo = new TransactionRepo();
            transactionRepo.whereAllUserId(currentUser.id);
            WriteCsv writeCsv = new WriteCsv("out.csv");
            transactionRepo.getAll().forEach(transaction -> writeCsv.writeLine(transaction));

            nextAction = new MainAction("Export completed to out.csv.");
            nextAction.currentUser = currentUser;
            nextAction.run();
        } catch (Exception ex) {

        }

    }
}
