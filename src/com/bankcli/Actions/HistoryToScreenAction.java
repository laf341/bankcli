package com.bankcli.actions;

import com.bankcli.repository.TransactionRepo;

public class HistoryToScreenAction extends Action {
    private TransactionRepo transactionRepo;

    public HistoryToScreenAction() {
        actionName = "On Screen";
    }

    public void run() {
        try {
            transactionRepo = new TransactionRepo();
            transactionRepo.whereAllUserId(currentUser.id);

            System.out.println("   Date                   From      To    Amount   ");
            System.out.println("----------------------------------------------------");
            transactionRepo.getAll().forEach(transaction -> {
                transaction.forEach(value -> System.out.print("|  " + value + "  "));
                System.out.println("\n----------------------------------------------------");
            });

            nextAction = new MainAction("Export completed on Screen. ");
            nextAction.currentUser = currentUser;
            nextAction.run();
        } catch (Exception ex) {

        }

    }
}
