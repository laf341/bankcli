package com.bankcli.actions;

import com.bankcli.entity.Transaction;
import com.bankcli.repository.TransactionRepo;
import com.bankcli.repository.UserRepo;


public class TranferMoneyAction extends Action {
    private UserRepo userTo;
    private TransactionRepo transactionRepo;
    public TranferMoneyAction() {
        setActionName("Transfer money by id");
        setHelloMessage("Enter userid:");
    }

    @Override
    public void run(){
        try {
            printHelloMessage();
            int userid = Integer.valueOf(bufferedReader.readLine());
            userTo = new UserRepo();
            userTo.where("id", "" + userid);
            if (userTo.getOne() == null) {
                this.error("User not found! Try again");
            }
            System.out.println("Enter sum:");
            int sum = Integer.valueOf(bufferedReader.readLine());
            transactionRepo = new TransactionRepo(new Transaction(currentUser, userTo.getOne(), sum));
            transactionRepo.save();

            nextAction = new MainAction("Transfer completed. ");
            nextAction.currentUser = currentUser;
            nextAction.run();

        } catch (NumberFormatException e) {
            this.error("Invalid input format!");
        }catch (Exception ex){
        }
    }
}
