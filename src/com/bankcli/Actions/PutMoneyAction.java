package com.bankcli.actions;


import com.bankcli.entity.Transaction;
import com.bankcli.repository.TransactionRepo;
import com.bankcli.repository.UserRepo;


public class PutMoneyAction extends Action {
    private UserRepo userTo;
    private TransactionRepo transactionRepo;

    public PutMoneyAction() {
        actionName = "Put money";
        helloMessage = "Enter sum:";
    }

    public void run() {
        super.run();
        try {
            int sum = Integer.parseInt(bufferedReader.readLine());

            userTo = new UserRepo();
            userTo.where("username", "bank__");
            transactionRepo = new TransactionRepo(new Transaction(currentUser, userTo.getOne(), sum));
            transactionRepo.save();

            nextAction = new MainAction("Putting completed. ");
            nextAction.currentUser = currentUser;
            nextAction.run();

        } catch (NumberFormatException e) {
            error("Invalid input format!");
        } catch (Exception ex) {

        }

    }
}
