package com.bankcli.actions;

class MainAction extends Action {
    private LogoutAction logoutAction = new LogoutAction();
    private TranferMoneyAction tranferMoneyAction = new TranferMoneyAction();
    private PutMoneyAction putMoneyAction = new PutMoneyAction();
    private TakeMoneyAction takeMoneyAction = new TakeMoneyAction();
    private ExportHistoryAction exportHistoryAction = new ExportHistoryAction();

    public MainAction() {
        helloMessage = "Choose action:";
        actionName = "Main menu";
        setActionInMap("1", tranferMoneyAction);
        setActionInMap("2", putMoneyAction);
        setActionInMap("3", takeMoneyAction);
        setActionInMap("4", exportHistoryAction);
        setActionInMap("5", logoutAction);
    }

    protected MainAction(String lastMessage) {
        helloMessage = lastMessage + "Choose action:";
        actionName = "Main menu";
        setActionInMap("1", tranferMoneyAction);
        setActionInMap("2", putMoneyAction);
        setActionInMap("3", takeMoneyAction);
        setActionInMap("4", exportHistoryAction);
        setActionInMap("5", logoutAction);
    }

    public void run(){
        try {
            printHelloMessage();
            printActions();
            inputAction = bufferedReader.readLine();
            if (!actionMap.containsKey(inputAction)) {
                this.error("Invalid input! Action not found");
            }
            actionMap.get(inputAction).currentUser = currentUser;
            actionMap.get(inputAction).run();
        } catch (Exception ex) {
        }

    }
}
