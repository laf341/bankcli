package com.bankcli.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReaderCsv extends CSV {
    private BufferedReader bufferedReader;

    public ReaderCsv(String file) throws Exception {
        super(file);
        bufferedReader = new BufferedReader(new FileReader(this.file));
    }

    public List<List<String>> getFileLines() throws Exception {
        List<List<String>> Line = new ArrayList<>();
        String str;
        while ((str = bufferedReader.readLine()) != null){
            String[] list = str.split(";");
            List<String> itemList = new ArrayList<>(Arrays.asList(list));
            Line.add(itemList);
        }
        bufferedReader.close();
        return Line;
    }

    // Получить строки из файла с упоминанием data в конкретной позиции (столбце)
    public List<List<String>> getLineByData(String data, int position) {
        try {
            List<List<String>> linesByData = new ArrayList<>();
            getFileLines().stream()
                    .filter(line -> line.indexOf(data) == position)
                    .forEach(line -> linesByData.add(line));
            bufferedReader.close();
            return linesByData;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    // Получить строки из файла с упоминанием data в нескольких позициях (столбцах)
    public List<List<String>> getLineByData(String data, List<Integer> positions) throws Exception {
        List<List<String>> linesByData = new ArrayList<>();
        getFileLines().stream()
                .filter(line -> positions.contains(line.indexOf(data)))
                .forEach(line -> linesByData.add(line));
        bufferedReader.close();
        return linesByData;
    }

}
