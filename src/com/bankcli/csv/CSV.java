package com.bankcli.csv;

import com.bankcli.general.Config;

public abstract class CSV{
    public String file;
    public CSV(String file) {
        this.file = Config.PATH_DB_FILES+file;
    }

}
