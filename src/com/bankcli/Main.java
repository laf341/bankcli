package com.bankcli;

import com.bankcli.entity.User;
import com.bankcli.general.Listening;
import com.bankcli.repository.UserRepo;

import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws Exception {
        /*
         * Генерация БД users.csv
         */
//        List<User> usersSeed = new ArrayList<>();
//        {
//            usersSeed.add(new User(0, "bank__", "0"));
//            usersSeed.add(new User(11011, "admin", "123"));
//            usersSeed.add(new User(11012, "frodo", "321"));
//        }
//        usersSeed.forEach(l -> {
//            try {
//                new UserRepo(l).save();
//            } catch (Exception ex){
//            }
//        });

        Listening listening = new Listening();
        listening.run();
    }
}
