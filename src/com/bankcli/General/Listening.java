package com.bankcli.general;

import com.bankcli.actions.DefaultAction;


public class Listening {
    public void run() throws Exception {
        DefaultAction defaultAction = new DefaultAction();
        do {
            defaultAction.run();
        } while (true);

    }
}
