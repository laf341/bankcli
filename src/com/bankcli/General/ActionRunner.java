package com.bankcli.General;

/**
 * Created by Александр on 04.04.2016.
 */
public interface ActionRunner {
    void runAction();
}
