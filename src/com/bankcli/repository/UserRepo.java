package com.bankcli.repository;

import com.bankcli.entity.User;
import com.bankcli.csv.ReaderCsv;
import com.bankcli.csv.WriteCsv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepo {
    public String file = "users.csv";
    public Map<String, Integer> position = new HashMap<>();

    {
        position.put("id", 0);
        position.put("username", 1);
        position.put("password", 2);
    }

    User user;

    public UserRepo() {
        this.user = new User();
    }

    public UserRepo(User user) {
        this.user = user;
    }

    public void where(String nameData, String data) {
        try {
            ReaderCsv readerCsv = new ReaderCsv(file);
            List<List<String>> userDatas = readerCsv.getLineByData(data, position.get(nameData));

            if (!userDatas.isEmpty()) {
                List<String> userData = userDatas.get(0);
                user.setId(Integer.parseInt(userData.get(0)));
                user.setName(userData.get(1));
                user.setPassword(userData.get(2));
            } else {
                user = null;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }


    }

    public void save() throws Exception {
        List<String> lineData = new ArrayList<>();
        {
            lineData.add(Integer.toString(user.getId()));
            lineData.add(user.getName());
            lineData.add(user.getPassword());
        }
        WriteCsv writeCsv = new WriteCsv(file);
        writeCsv.writeLine(lineData);
    }

    public User getOne() {
        return user;
    }
}
