package com.bankcli.repository;


import com.bankcli.csv.WriteCsv;
import com.bankcli.csv.ReaderCsv;
import com.bankcli.entity.Transaction;

import java.text.SimpleDateFormat;
import java.util.*;

public class TransactionRepo {
    public String file = "transactions.csv";
    public Map<String, Integer> position = new HashMap<>();

    {
        position.put("datetime", 0);
        position.put("from", 1);
        position.put("to", 2);
        position.put("amount", 3);
    }

    private SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    private List<List<String>> transactionDatas;
    private Transaction transaction;

    public TransactionRepo() {
        this.transaction = new Transaction();
    }

    public TransactionRepo(Transaction transaction) {
        this.transaction = transaction;
    }

    public void where(String nameData, String data) throws Exception {
        ReaderCsv readerCsv = new ReaderCsv(file);
        transactionDatas = readerCsv.getLineByData(data, position.get(nameData));
    }

    public void whereAllUserId(int userid) throws Exception {
        ReaderCsv readerCsv = new ReaderCsv(file);
        List<Integer> positions = new ArrayList<>();
        {
            positions.add(position.get("from"));
            positions.add(position.get("to"));
        }

        transactionDatas = readerCsv.getLineByData("" + userid, positions);
    }

    public void save() {
        try {
            List<String> lineData = new ArrayList<>();
            {
                lineData.add(ft.format(transaction.datetime));
                lineData.add("" + transaction.from.id);
                lineData.add("" + transaction.to.id);
                lineData.add("" + transaction.amount);
            }
            WriteCsv writeCsv = new WriteCsv(file);
            writeCsv.writeLine(lineData);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public Transaction getOne() throws Exception {
        List<String> transactionData = transactionDatas.get(0);
        transaction.datetime = ft.parse(transactionData.get(position.get("datetime")));
        UserRepo userInfo = new UserRepo();
        userInfo.where("id", transactionData.get(position.get("from")));
        transaction.from = userInfo.getOne();
        userInfo.where("id", transactionData.get(position.get("to")));
        transaction.to = userInfo.getOne();
        transaction.amount = Integer.valueOf(transactionData.get(position.get("amount")));
        return transaction;
    }

    public List<List<String>> getAll() {
        return transactionDatas;
    }
}
